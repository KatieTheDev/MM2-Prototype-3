﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Scorekeeper
{
    // Player control keybinds
    public static KeyCode restartKey = KeyCode.R;
    public static KeyCode jumpKey = KeyCode.Space;
    public static KeyCode runKey = KeyCode.LeftControl;

    // Scorekeeping variables
    public static bool isAlive = true;
    public static int score;

    // Movement and gameplay values
    public static bool gameRestarted = false;
    public static bool playerRunning = false;
    public static bool startingAnimationEnded = false;
    public static bool playerOnGround = true;
    public static float runningMultiplier = 1.0f;
    public static float jumpForce = 10;
    public static int jumpCount = 0;

    public static void KillPlayer()
    {
        isAlive = false;
        Debug.Log("Game over. Press " + restartKey + " to try again. Final score: " + score);
    }

    public static void RestartGame()
    {
        isAlive = true;
        score = 0;
        Debug.Log("Game restarted.");
        gameRestarted = true;
        jumpCount = 0;
        startingAnimationEnded = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Add score to player
    public static void AddScore(int addValue, string addReason = null)
    {
        score += addValue;
        if (!string.IsNullOrWhiteSpace(addReason))
        {
            Debug.Log(addValue + " has been added to the score for " + addReason + ". New score: " + score);
        }
        else if (string.IsNullOrWhiteSpace(addReason))
        {
            Debug.Log(addValue + " has been added to the score. New score: " + score);
        }
    }

    // Remove score from player
    public static void ScorePenalty(int penaltyValue, string penaltyReason)
    {
        score -= penaltyValue;
        Debug.Log("Score has been penalized by " + penaltyValue + "for " + penaltyReason + ". New score: " + score);
    }

    public static void PlayerRunning(bool isRunning)
    {
        if (isRunning == true)
        {
            runningMultiplier = 2;
        }
        else if (isRunning == false)
        {
            runningMultiplier = 1;
        }
    }
}
