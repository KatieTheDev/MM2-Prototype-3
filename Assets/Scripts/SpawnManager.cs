﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] obstaclePrefabs;
    private Vector3 spawnpos = new Vector3(25.60326f, -5.312479f, 7.616999f);
    private readonly float startDelay = 2;
    private readonly float repeatRate = 2;
    private int obstacleIndex = 0;
    //private Vector3 positionArray[] = [];

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObstacle", startDelay, repeatRate);
    }

    void SpawnObstacle()
    {
        if (Scorekeeper.startingAnimationEnded)
        {
            // Determine start position
            obstacleIndex = Random.Range(0, obstaclePrefabs.Length);
            if (obstacleIndex == 0)
            {
                spawnpos = new Vector3(25.60326f, -5.312479f, 7.616999f);
            }
            else if (obstacleIndex == 1)
            {
                spawnpos = new Vector3(25.60326f, -4.1574f, 2.016937f);
            }
            else if (obstacleIndex == 2)
            {
                spawnpos = new Vector3(25.60326f, -3.630494f, -0.4691813f);
            }

            if (Scorekeeper.isAlive && !Scorekeeper.gameRestarted)
            {
                Instantiate(obstaclePrefabs[obstacleIndex], spawnpos, obstaclePrefabs[obstacleIndex].transform.rotation);
            }
            else if (Scorekeeper.isAlive && Scorekeeper.gameRestarted)
            {
                // Wait 30 frames before starting to spawn again
                int waitTimer = 0;
                int waitLength = 30;
                while (waitTimer < waitLength)
                {
                    waitTimer++;
                }
                if (waitTimer >= waitLength)
                {
                    Instantiate(obstaclePrefabs[obstacleIndex], spawnpos, obstaclePrefabs[obstacleIndex].transform.rotation);
                }
            }
        }
    }
}
