﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObstacle : MonoBehaviour
{
    // Set speed moving left
    private float speed = 20.0f;
    private float leftBound = -15;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Scorekeeper.isAlive && Scorekeeper.startingAnimationEnded)
        {
            transform.Translate(speed * Scorekeeper.runningMultiplier * Time.deltaTime * Vector3.left);
        }
        if ((transform.position.x < leftBound && gameObject.CompareTag("Obstacle")) && Scorekeeper.startingAnimationEnded)
        {
            Destroy(gameObject);
        }
        if (Scorekeeper.gameRestarted)
        {
            int waitTimer = 0;
            int waitGoal = 10;
            Destroy(gameObject);
            while (waitTimer < waitGoal)
            {
                waitTimer++;
            }
            if (waitTimer >= waitGoal)
            {
                Scorekeeper.gameRestarted = false;
            }
        }
    }
}
