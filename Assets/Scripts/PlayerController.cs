﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Initalize all script-only variables
    // All movement keys and other variables are stored in Scorekeeper.cs
    private Rigidbody playerRB;
    private Animator playerAnim;
    public ParticleSystem explosionParticle;
    public ParticleSystem dirtParticle;
    public AudioClip crashSound;
    public AudioClip jumpSound;
    private AudioSource playerAudio;

    // Create starting animation timer variables
    private int startAnimTimerLength = 10; // Total length of wait timer
    private int startAnimTimerCurrent = 0; // Current value of wait timer

    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        playerAnim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Run starting animation if it has not been run already
        if (!Scorekeeper.startingAnimationEnded)
        {
            StartingAnimation();
        }

        // Jump if on the ground or if you have only jumped once, whenever you press the space key
        if (Input.GetKeyDown(Scorekeeper.jumpKey) && (Scorekeeper.playerOnGround || Scorekeeper.jumpCount < 2) && Scorekeeper.isAlive)
        {
            MakeJump();
        }

        // Use left ctrl key to make player run
        if (Input.GetKey(Scorekeeper.runKey))
        {
            ChangeSpeedMultiplier(2);
        }
        else if (!Input.GetKey(Scorekeeper.runKey))
        {
            ChangeSpeedMultiplier(1);
        }

        // Move the player to the correct position so they cannot get lost from the middle of the 
        if ((transform.position.x != 5 || transform.position.z != 0) && Scorekeeper.startingAnimationEnded)
        {
            transform.position = new Vector3(5, transform.position.y, 0);
        }

        // Restart game when the restart key is pressed
        if (Scorekeeper.isAlive == false && Input.GetKeyDown(Scorekeeper.restartKey) && Scorekeeper.startingAnimationEnded)
        {
            Scorekeeper.RestartGame(); // Completely restarts game
        }

        // Add score every frame
        if (Scorekeeper.isAlive && Scorekeeper.startingAnimationEnded)
        {
            Scorekeeper.AddScore(1); // Adds passed value to score
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground") && Scorekeeper.isAlive)
        {
            Scorekeeper.playerOnGround = true;
            Scorekeeper.jumpCount = 0;
            dirtParticle.Play();
        }
        else if (collision.gameObject.CompareTag("Obstacle") && Scorekeeper.isAlive)
        {
            KillPlayer();
        }
    }

    // Code to run when player dies
    private void KillPlayer()
    {
        Scorekeeper.KillPlayer();
        dirtParticle.Stop();
        playerAnim.SetBool("Death_b", true);
        playerAnim.SetInteger("DeathType_int", 1);
        playerAudio.PlayOneShot(crashSound, 1.0f);
        explosionParticle.Play();
        transform.position = new Vector3(5, 0, 0);
    }

    // Make player jump
    private void MakeJump()
    {
        float forceMultiplier = 1;
        if (Scorekeeper.jumpCount == 0)
        {
            forceMultiplier = 1;
        }
        else if (Scorekeeper.jumpCount == 1)
        {
            forceMultiplier = 0.5f;
        }
        playerRB.AddForce(Vector3.up * Scorekeeper.jumpForce * forceMultiplier, ForceMode.Impulse);
        Scorekeeper.playerOnGround = false;
        playerAnim.SetTrigger("Jump_trig");
        dirtParticle.Stop();
        playerAudio.PlayOneShot(jumpSound, 1.0f);
        Scorekeeper.jumpCount++;
    }

    // Starting animation
    private void StartingAnimation()
    {
        // Set correct settings for running
        ChangeSpeedMultiplier(1, 0);

        // Move player right
        transform.Translate(new Vector3(1, 0, 0) * -30000 * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, 0, 0);

        if (transform.position.x >= 5)
        {
            transform.position = new Vector3(5, 0, 0);
        }

        startAnimTimerCurrent++;

        // End animation if in correct position
        if (transform.position == new Vector3(5, transform.position.y, transform.position.z) && startAnimTimerCurrent >= startAnimTimerLength)
        {
            // End animation
            ChangeSpeedMultiplier(1);
            Scorekeeper.startingAnimationEnded = true; // Set starting animation ended so rest of game can continue
        }
    }

    // Change speed easily
    private void ChangeSpeedMultiplier(float newMultiplier, float runSpeed = float.NaN)
    {
        if (float.IsNaN(runSpeed)) {
            playerAnim.SetFloat("AnimSpeed", newMultiplier); // Change speed on animation
            Scorekeeper.runningMultiplier = newMultiplier; // Change speed on screen
        }
        else if (!float.IsNaN(runSpeed))
        {
            playerAnim.SetFloat("AnimSpeed", newMultiplier); // Change speed on animation
            Scorekeeper.runningMultiplier = runSpeed; // Change speed on screen
        }
        
    }
}
